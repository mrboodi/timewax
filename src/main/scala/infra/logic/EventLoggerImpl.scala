package infra.logic

import cats.effect.IO
import infra.{Event, EventLogger}

class EventLoggerImpl private(var source: List[Event]) extends EventLogger {
  def event(info: String): IO[Unit] = IO.delay {
    source = Event.create(info) :: source
  }

  def events: IO[List[Event]] = IO.pure(source.reverse)
}

object EventLoggerImpl {
  def apply(): EventLoggerImpl = new EventLoggerImpl(Nil)
}
