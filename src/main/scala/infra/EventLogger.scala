package infra
import cats.effect.IO
import org.joda.time.DateTime

final case class Event private(info: String, dateTime: DateTime)

object Event {
  def create(info: String): Event = Event(info, DateTime.now())
}

trait EventLogger {
  def event(info: String): IO[Unit]
  def events: IO[List[Event]]
}
