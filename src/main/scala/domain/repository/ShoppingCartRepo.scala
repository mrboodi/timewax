package domain.repository

import cats.effect.IO
import domain.model.{Customer, ShoppingCart}

trait ShoppingCartRepo {
//  def findByCustomer(customer: Customer): IO[Option[ShoppingCart]]
  def persist(shoppingCart: ShoppingCart): IO[Unit]
}