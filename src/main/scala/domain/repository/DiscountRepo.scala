package domain.repository

import cats.effect.IO
import domain.model.Discount

trait DiscountRepo {
  def findByCode(code: String): IO[Option[Discount]]
}