package domain.repository

import cats.data.Kleisli
import cats.effect.IO
import domain.model.{Item, ItemView, ShoppingCart}

trait CartItemRepo {
  type Operation[A] = Kleisli[IO, ShoppingCart, A]

  def insert(item: Item): Operation[ItemView]
  def remove(itemId: String): Operation[Unit]
}