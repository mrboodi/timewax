package domain.repository.logic

import cats.data.Kleisli
import cats.effect.IO
import domain.model.{Item, ItemView}
import domain.repository.CartItemRepo

class CartItemRepoImpl private() extends CartItemRepo {
  def insert(item: Item): Operation[ItemView] = Kleisli( cart => IO.delay {
    val view = ItemView(cart.items.size.toString, cart.id, item)
    cart.items = cart.items + view
    view
  })

  def remove(itemId: String): Operation[Unit] = Kleisli(cart => IO.delay {
    cart.items = cart.items.filterNot(_.id == itemId)
  })
}

object CartItemRepoImpl {
  def apply(): CartItemRepo = new CartItemRepoImpl()
}
