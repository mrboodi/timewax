package domain.repository.logic

import cats.effect.IO
import domain.model.Discount
import domain.repository.DiscountRepo

class DiscountRepoImpl private(list: List[Discount] = Nil) extends DiscountRepo {
  def findByCode(code: String): IO[Option[Discount]] = IO.delay {
    list.find(_.code == code)
  }
}

object DiscountRepoImpl {
  def apply(list: List[Discount]): DiscountRepo = new DiscountRepoImpl(list)
}
