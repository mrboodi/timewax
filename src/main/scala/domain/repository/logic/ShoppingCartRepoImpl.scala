package domain.repository.logic

import cats.effect.IO
import domain.model.{Customer, ShoppingCart}
import domain.repository.ShoppingCartRepo

class ShoppingCartRepoImpl private() extends ShoppingCartRepo {
  def persist(shoppingCart: ShoppingCart): IO[Unit] = IO.delay {
    //Yeah! About that: I am just abusing FP with use of mutability :D So ... maybe next time!
  }
}

object ShoppingCartRepoImpl {
  def apply(): ShoppingCartRepo = new ShoppingCartRepoImpl()
}
