package domain.model

//An indexed row representation
final case class ItemView(id: String, cartId: String, item: Item)

//Yeah! I cheated with that "var" :D But you can also imagine this being a denormalized NoSQL model!
final case class ShoppingCart(id: String, customerId: String, var discount: Option[Discount], var items: Set[ItemView] = Set())

object ShoppingCart {
  //Not so proud here!
  def setDiscount(cart: ShoppingCart, dc: Discount): ShoppingCart = {
    cart.discount = Some(dc); cart
  }
}

