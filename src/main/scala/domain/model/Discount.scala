package domain.model

final case class Discount private(code: String, percentage: Int)

object Discount {
  def create(code: String, percentage: Int): Option[Discount] =
    if (percentage > 0 && percentage <= 100) Option(Discount(code, percentage))
    else None
}
