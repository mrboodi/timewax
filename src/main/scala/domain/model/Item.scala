package domain.model

/*
 *  https://en.wikipedia.org/wiki/Stock_keeping_unit
 *  An entity which represents SKU but since is this a prototype I'll be ignoring its details
 */
final case class SKU(description: String)

//A data model for Item
final case class Item private(quantity: Int, displayName: String, price: BigDecimal, sku: SKU)

object Item {
  def create(quantity: Int, displayName: String, price: BigDecimal, sku: SKU): Option[Item] =
    if (quantity > 0 && quantity < 1001) Option(Item(quantity, displayName, price, sku))
    else None

  def updateQuantity(item: Item): Int => Option[Item] = { quantity =>
    Item.create(quantity, item.displayName, item.price, item.sku)
  }
}
