package domain.story.logic

import cats.data.Kleisli
import cats.effect.IO
import cats.implicits._
import domain.model.{Item, ShoppingCart}
import domain.repository.{CartItemRepo, DiscountRepo, ShoppingCartRepo}
import domain.story.ShoppingStory
import infra.EventLogger

class ShoppingStoryImpl private(
  discountRepo: DiscountRepo,
  shoppingCartRepo: ShoppingCartRepo,
  cartItemRepo: CartItemRepo,
  eventLogger: EventLogger
) extends ShoppingStory {

  def order: Operation[Unit] = ???

  def applyDiscount(discount: String): Operation[Either[String, Unit]] = Kleisli { cart =>
    discountRepo.findByCode(discount).flatMap {
      case Some(discount) =>
        shoppingCartRepo
          .persist(ShoppingCart.setDiscount(cart, discount)) *>
          eventLogger.event(s"Apply $discount to $cart") *>
          IO.pure(Right(()))
      case None =>
        eventLogger.event(s"Apply invalid $discount to $cart") *>
          IO.pure("Discount is invalid".asLeft[Unit])
    }
  }

  def totalValue: Operation[BigDecimal] = Kleisli(cart => IO.delay {
    val sum = cart.items.map(view => view.item.price * view.item.quantity).sum
    val discount = cart.discount.map(_.percentage).getOrElse(0)
    sum - (sum / 100 * discount)
  })

  def addItem(item: Item): Operation[Unit] =
    cartItemRepo.insert(item) *> Kleisli(cart => eventLogger.event(s"Add $item to $cart"))

  def removeItem(id: String): Operation[Unit] =
    cartItemRepo.remove(id) *> Kleisli(cart => eventLogger.event(s"Remove $id item from $cart"))

  def empty: Operation[Unit] = ???
}

object ShoppingStoryImpl {
  def apply(
    discountRepo: DiscountRepo,
    shoppingCartRepo: ShoppingCartRepo,
    cartItemRepo: CartItemRepo,
    eventLogger: EventLogger
  ): ShoppingStory =
    new ShoppingStoryImpl(discountRepo, shoppingCartRepo, cartItemRepo, eventLogger)
}
