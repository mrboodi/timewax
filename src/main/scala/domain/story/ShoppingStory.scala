package domain.story

import cats.data.Kleisli
import cats.effect.IO
import domain.model.{Item, ShoppingCart}

//As a customer I'd like to...
trait ShoppingStory {
  type Operation[A] = Kleisli[IO, ShoppingCart, A]

  def order: Operation[Unit]
  def applyDiscount(discount: String): Operation[Either[String, Unit]]
  def totalValue: Operation[BigDecimal]
  def addItem(item: Item): Operation[Unit]
  def removeItem(id: String): Operation[Unit]
  def empty: Operation[Unit]
}
