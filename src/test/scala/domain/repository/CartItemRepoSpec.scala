package domain.repository

import domain.model.{Item, SKU, ShoppingCart}
import domain.repository.logic.CartItemRepoImpl
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CartItemRepoSpec extends AnyFlatSpec with Matchers  {
  val repo = CartItemRepoImpl()
  val cart = ShoppingCart("1", "2", None, items = Set())

  val item = Item.create(1, "hey", 100, SKU("h")).get

  it should "Store an item" in {
    repo.insert(item)(cart).unsafeRunSync().item shouldBe item
  }

  it should "remove an item" in {
    cart.items.size shouldBe 1
    repo.remove(cart.items.head.id)(cart).unsafeRunSync()
    cart.items.size shouldBe 0
  }
}
