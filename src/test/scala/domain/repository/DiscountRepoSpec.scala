package domain.repository

import domain.model.Discount
import domain.repository.logic.DiscountRepoImpl
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class DiscountRepoSpec extends AnyFlatSpec with Matchers   {
  val listOfDiscounts = List(
    Discount.create("code1", 100),
    Discount.create("code2", 10),
    Discount.create("code3", 5),
    Discount.create("code4", 2),
    Discount.create("code5", 17),
  ).collect { case Some(value) => value }

  val repo = DiscountRepoImpl(listOfDiscounts)
  it should "find a discount by its code" in {
    repo.findByCode("code1").unsafeRunSync() shouldBe Some(listOfDiscounts(0))
  }

  it should "not find if it's not there" in {
    repo.findByCode("code10").unsafeRunSync() shouldBe None
  }
}
