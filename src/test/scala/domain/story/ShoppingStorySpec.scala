package domain.story

import domain.model.{Discount, Item, ItemView, SKU, ShoppingCart}
import domain.repository.logic.{CartItemRepoImpl, DiscountRepoImpl, ShoppingCartRepoImpl}
import domain.story.logic.ShoppingStoryImpl
import infra.{Event, EventLogger}
import infra.logic.EventLoggerImpl
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ShoppingStorySpec extends AnyFlatSpec with Matchers {
  val listOfDiscounts = List(
    Discount.create("code1", 100),
    Discount.create("code2", 10),
    Discount.create("code3", 5),
    Discount.create("code4", 2),
    Discount.create("code5", 17),
  ).collect { case Some(value) => value }

  val discountRepo = DiscountRepoImpl(listOfDiscounts)
  val eventLogger: EventLogger = EventLoggerImpl()
  val shoppingCartRepo = ShoppingCartRepoImpl()
  val cartItemRepo = CartItemRepoImpl()
  val shoppingStory = ShoppingStoryImpl(discountRepo, shoppingCartRepo, cartItemRepo, eventLogger)

  val item = Item.create(2, "honey", 1000, SKU("Mmm!")).get
  val cart = ShoppingCart("1", "c1", None, Set())

  eventLogger.events.unsafeRunSync().isEmpty shouldBe true

  "A Shopping cart" should "be ordered" in {
    //shoppingStory.order
  }

  it should "add a new item" in {
    val item = Item.create(2, "honey", 1000, SKU("Mmm!")).get
    val cart = ShoppingCart("1", "c1", None, Set())
    shoppingStory.addItem(item)(cart).attempt.unsafeRunSync().isRight shouldBe true
    cart.items.map(_.item) shouldBe Set(item)
  }

  it should "remove an item" in {
    shoppingStory.addItem(item)(cart).attempt.unsafeRunSync().isRight shouldBe true
    cart.items.map(_.item) shouldBe Set(item)
    shoppingStory.removeItem(cart.items.head.id)(cart).attempt.unsafeRunSync().isRight shouldBe true
    cart.items shouldBe Set()
  }


  it should "calculate its total value" in {
    shoppingStory.addItem(item)(cart).attempt.unsafeRunSync().isRight shouldBe true
    cart.items.map(_.item) shouldBe Set(item)
    shoppingStory.totalValue(cart).unsafeRunSync().toInt shouldBe 2000
  }

  it should "apply a discount and change the total fee" in {
    shoppingStory.applyDiscount("code2")(cart).unsafeRunSync().isRight shouldBe true
    shoppingStory.totalValue(cart).unsafeRunSync().toInt shouldBe 1800
  }

  it should "empty itself" in {
    //Maybe later?!
  }

  it should "generate events" in {
    eventLogger.events.unsafeRunSync().isEmpty shouldBe false
  }
}
