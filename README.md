Hi,

This is a solution implemented in a few hours and is not complete and is not meant to be used for production in anyway!
There are tricks used to speed up the implementation like mutability which is not acceptable in anyway.
Parameterization of IO has been avoided to lessen the amount of work needed to be done and make the project less complicated.
Tests are not complete... because the purpose is to show how one would design an application and a DDD structure.

Onion architecture has been used just because I personally find it easier to decouple units(however you want to define them, i'll go with context).

Just thought I had write these notes to make sure there are no misunderstandings.
